/**
 * @file   ScramblerTest.cpp
 * @author Jim Daehn
 * @brief  Implementation of Scrambler Unit Test. DO NOT MODIFY THE CONTENTS 
 * OF THIS FILE! ANY MODIFICATION TO THIS FILE WILL RESULT IN A GRADE OF 0 FOR
 * THIS LAB!
 */

#include "ScramblerTest.h"


CPPUNIT_TEST_SUITE_REGISTRATION(ScramblerTest);

ScramblerTest::ScramblerTest() {
}

ScramblerTest::~ScramblerTest() {
}

void ScramblerTest::setUp() {
    scrambler = new Scrambler(initData, 0, 3);
}

void ScramblerTest::tearDown() {
    delete scrambler;
    scrambler = nullptr;
}

void ScramblerTest::testLength() {
    int expected = 3;
    int actual = scrambler->length();

    CPPUNIT_ASSERT_EQUAL(expected, actual);
}

void ScramblerTest::testScramble() {
    std::string expected = "cba";
    std::string actual = scrambler->scramble();
    CPPUNIT_ASSERT_EQUAL(expected, actual);
}

void ScramblerTest::testToString() {
    std::string expected = "abc";
    std::string actual = scrambler->toString();
    CPPUNIT_ASSERT_EQUAL(expected, actual);
}
