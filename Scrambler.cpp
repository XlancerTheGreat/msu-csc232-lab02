/**
 * @file    Scrambler.cpp
 * @authors Joseph Sneddon and Robert Dascanio
 * @brief   Scrambler implementation.
 */


#include <sstream>

#include "Scrambler.h"

#include <string>

Scrambler::Scrambler(const char a[], size_t beginIndex, size_t endIndex)
    : begin(beginIndex), end(endIndex) {
		array[end - begin];
		for (int i = begin; i < end; i++ )
		{
			array[i] = a[i];
		}
		
    // Copies the given array into the data member array
}

int Scrambler::length() const {
    //Returns the length of actual array elements in use.
    return end - begin;
}

std::string Scrambler::scramble() {
    // Calls reverse method 
    return reverse(array, begin, end);
}

std::string Scrambler::toString() const {
	// Creates a std::string representation of the character data stored
    // by this Scrambler.
	std::string string;
	for (int i = begin; i < end; i++ )
		{
			string+= array[i];
		}
    return string;
}


Scrambler::~Scrambler() {
   //Don't edit me please.
}

// Private data member implementations
std::string Scrambler::reverse(const char anArray[], int first, int last) {
	//Creates a recursive function that reverses the elements in the array.
	if (first<last)
	{
		return anArray[last] + reverse(anArray, first, --last);
	}
	else
		return "";
}
