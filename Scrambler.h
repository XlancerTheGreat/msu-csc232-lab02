/**
 * @file    Scrambler.h
 * @authors Joseph Sneddon and Robert Dascanio 
 * @brief  Scrambler specification.
 */

#ifndef SCRAMBLER_H
#define SCRAMBLER_H

#include <iostream>
#include <string>

static const int MAX_SIZE = 80;

class Scrambler {
public:
    /**
     * Parameterized constructor that provides this Scrambler with an array of
     * character data contained in the cells defined by [beginIndex, endIndex).
     *
     * @pre   beginIndex is greater than or equal to 0 and endIndex is less than
     *        or equal to MAX_SIZE.
     * @param a an initial character array stored by this Scrambler
     * @param beginIndex the index of the given array where the data begins
     * @param endIndex the index of the given array where the data ends; this
     *                 is the next "free" index after the data
     */
    Scrambler(const char a[], size_t beginIndex, size_t endIndex);

    /**
     * Get the number of characters actually stored by this Scrambler.
     *
     * @return The number of characters actually stored by this Scramber is
     *         returned.
     */
    int length() const;

    /**
     * Return a "scrambled" form of the characters stored by this Scrambler.
     * For this lab, "scrambled" is nothing more than reversed.
     *
     * @post   The stored character data remains unchanged, i.e., a call to
     *         toString() after a call to this method would return the original
     *         character data in its original order.
     * @return A string form of the character data stored by this Scrambler,
     *         in reverse order, is returned.
     */
    std::string scramble();

    /**
     * Return a string representation of the character data stored by this
     * Scrambler.
     *
     * @return A string representation of the character data stored by this
     *         Scrambler is returned.
     */
    std::string toString() const;

    /**
     * Scrambler destructor.
     */
    virtual ~Scrambler();
private:
    /**
     * A private "helper" method utilized by the reverse() opertion. This
     * helper method is to be written as a recursive method. See page 67
     * for a close facsimile to this method. The only difference is that the
     * method in the book prints directly to the standard ouput stream; this
     * method doesn't do that. Rather, it creates a string that is returned.
     *
     * @pre    The array anArray contains size characters, where size >= 0.
     * @post   None.
     * @param  anArray the array to reverse
     * @param  first the index of the first character in the array
     * @param  last the index that is 1 greater than the last character in the
     *              the array
     * @return A string representation of the character data in reverse is
     *         returned.
     */
    std::string reverse(const char anArray[], int first, int last);

    /* The character data stored by this Scrambler. */
    char array[MAX_SIZE];
    /* The index of the first character in the array. */
    size_t begin;
    /* The index that is 1 greater than the last character in the array. */
    size_t end;
};

#endif /* SCRAMBLER_H */
